//
//  NewsTableViewCell.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

protocol NewsTableViewCellDelegate {
    func newsBtnTapped(cell:NewsTableViewCell)
    func openBtnTapped(cell:NewsTableViewCell)
}

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var descriptionDetailLabel: UILabel!
    @IBOutlet weak var openBtn: UIButton!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var videoStackView: UIStackView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var decriptionLabel: UILabel!
    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    var isExpanded = false
    var indexPath:IndexPath!
    var delegate: NewsTableViewCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.addRoundShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func openBtnTapped(_ sender: Any) {
        delegate.openBtnTapped(cell: self)
    }
    @IBAction func expandTapped(_ sender: UIButton) {
        if(isExpanded){
            videoStackView.isHidden = true
            isExpanded = false
            favBtn.isHidden = true
            stackViewHeight.constant = 0.0
        }else{
            videoStackView.isHidden = false
            favBtn.isHidden = false
            isExpanded = true
            stackViewHeight.constant = 80
        }
        layoutIfNeeded()
        delegate.newsBtnTapped(cell: self)
    }
}
