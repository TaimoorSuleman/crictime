//
//  HomeTableViewCell.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

protocol HomeCellProtocol {
    func liveBtnTapped(cell:HomeTableViewCell)
}
class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var liveBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    var delegate :HomeCellProtocol!
    var indexPath: IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    func setup(){
        mainView.addRoundShadow()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func liveBtnTapped(_ sender: UIButton) {
        delegate.liveBtnTapped(cell: self)
    }
    
}
