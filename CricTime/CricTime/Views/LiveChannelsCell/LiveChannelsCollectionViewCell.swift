//
//  LiveChannelsCollectionViewCell.swift
//  CricTime
//
//  Created by Taimoor Suleman on 20/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class LiveChannelsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var channelImage: UIImageView!
    @IBOutlet weak var channelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainView.addRoundShadow()
    }

}
