//
//  NoJobsViews.swift
//  Labour Choice
//
//  Created by Umair on 07/07/2017.
//  Copyright © 2017 talha. All rights reserved.
//

import UIKit

class EmptyTableViewBackgroundView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!

    class func instanceFromNib() -> EmptyTableViewBackgroundView {
        return UINib(nibName: "EmptyTableViewBackgroundView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyTableViewBackgroundView
    }

}
