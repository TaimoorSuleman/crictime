//
//  FixtureTableViewCell.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class FixtureTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var team1Flag: UIImageView!
    @IBOutlet weak var team1Name: UILabel!
    
    @IBOutlet weak var team2Name: UILabel!
    @IBOutlet weak var team2Flag: UIImageView!
    @IBOutlet weak var matchDate: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var venue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.addRoundShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
