
import UIKit
import Device
import NVActivityIndicatorView

protocol ACBaseViewControllerSearchDelegate {
    
    func searchWith(term: String)
    func cancelSearch()
}

class BaseViewController: UIViewController, NVActivityIndicatorViewable {
    var leftSearchBarButtonItem : UIBarButtonItem?
    var rightSearchBarButtonItem : UIBarButtonItem?
    var logoImageView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.setHidesBackButton(true, animated: false)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.setHidesBackButton(true, animated: false)
//        addBackButtonToNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addBackButtonToNavigationBar(){
        self.leftSearchBarButtonItem =  UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = self.leftSearchBarButtonItem;
    }
    
    func addSettingsButtonToNavigationBar(){
        self.rightSearchBarButtonItem =  UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goToSettings))
        self.navigationItem.rightBarButtonItem = self.rightSearchBarButtonItem;
    }
    
    @objc func goToSettings(){
        let settingsVC = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    
    func startLoading(){
        let size = CGSize(width: 50, height:50)
        startAnimating(size, message: "", type:  NVActivityIndicatorType(rawValue: NVActivityIndicatorType.ballScaleMultiple.rawValue), color: Global.purpleColor,  textColor: Global.purpleColor)
    }
    

    func stopLoading(){
        stopAnimating()
    }
    
    
}
