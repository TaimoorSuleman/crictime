//
//  SettingsViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {

    @IBOutlet weak var favView: UIView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var momentView: UIView!
    @IBOutlet weak var notificationBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

    func setUp() {
        self.navigationItem.title = "Settings"
        self.addBackButtonToNavigationBar()
        self.notificationView.addRoundShadow()
        self.favView.addRoundShadow()
        self.momentView.addRoundShadow()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func notificationBtnTapped(_ sender: UIButton) {
        if(notificationBtn.image(for: .normal) == #imageLiteral(resourceName: "Toggle-On")){
            notificationBtn.setImage(#imageLiteral(resourceName: "Toggle-Off"), for: .normal)
        }else{
            notificationBtn.setImage(#imageLiteral(resourceName: "Toggle-On"), for: .normal)
        }
    }


}
