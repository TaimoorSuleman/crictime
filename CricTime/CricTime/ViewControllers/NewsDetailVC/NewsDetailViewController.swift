//
//  NewsDetailViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 14/09/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsDetailViewController: BaseViewController {

    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var descriptionText: UITextView!
    
    var newsData = NewsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.descriptionText.text = newsData.description
        self.titleImage.sd_setImage(with: URL(string:(newsData.image)!), placeholderImage: #imageLiteral(resourceName: "Splash"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }
    
    func setup(){
        self.navigationItem.title =  NSLocalizedString("News Detail", comment: "")
        self.navigationController?.isNavigationBarHidden = false
      
    }

    

}
