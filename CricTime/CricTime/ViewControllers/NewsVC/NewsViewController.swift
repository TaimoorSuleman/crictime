//
//  NewsViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import SDWebImage
import GoogleMobileAds

class NewsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = ResultModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(dataSource.data?.count == 0){
            getNews()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setup(){
        self.navigationItem.title =  NSLocalizedString("News", comment: "")
        self.navigationController?.isNavigationBarHidden = false
        //        self.addSettingsButtonToNavigationBar()
        tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getNews(){
        
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (response) in
            print(response)
            self.stopLoading()
            let decoder = JSONDecoder()
            do{
                self.dataSource = try decoder.decode(ResultModel.self, from: response)
                self.tableView.reloadData()
                print("abc")
            }catch{
                print("Error")
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
        }
        self.startLoading()
        let params:Parameters = ["cid": 10]
        APIManager.sharedInstance.getNews(parameters: params, success: successClosure, failure: failureClosure, errorPopup: false)
        
    }
}


//MARK:- UITableView Datasource
extension NewsViewController:  UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.dataSource.status == 0 {
            Utility.emptyTableViewMessageWithImage(message: "Currently no news available", title: "Oooops", viewController: self, tableView: tableView)
            return 0
        }
        tableView.backgroundView = UIView()
        let adds = (dataSource.data?.count)! / 4
        return dataSource.data!.count / adds
        
//        return 20
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (dataSource.data?.count)! / 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: NewsTableViewCell.self) as! NewsTableViewCell
        cell.indexPath = indexPath
        cell.delegate = self
       
//        let diff = (indexPath.row) / 4
//        let index = indexPath.row - diff
//        if(indexPath.row % 4 == 0 && indexPath.row != 0){
//            //show add
//            cell.titleLbl.text = "Add"
//            cell.decriptionLabel.text = "Add"
//            cell.descriptionDetailLabel.text = "Add"
//            cell.addSubview(adMobBannerView)
//        }else{
            let index = indexPath.section * 4 + indexPath.row % 5
            cell.favBtn.alpha = 0.0
            cell.titleLbl.text = dataSource.data![index].title
            cell.decriptionLabel.text = dataSource.data![index].description
            cell.descriptionDetailLabel.text = dataSource.data![index].description
            cell.subTitleLbl.text = dataSource.data![index].description?.components(separatedBy: " ").first
            cell.titleImage.sd_setImage(with: URL(string:(dataSource.data![index].image)! ), placeholderImage: #imageLiteral(resourceName: "Splash"))
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var adMobBannerView = GADBannerView()
        adMobBannerView = GADBannerView(adSize: kGADAdSizeLargeBanner)
        adMobBannerView.adUnitID = Constants.ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID,"0fed5962b9f8f0de5ea6d0b19864e36d"]
        adMobBannerView.load(request)
        return adMobBannerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100//adMobBannerView.frame.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}

extension NewsViewController : NewsTableViewCellDelegate{
    func newsBtnTapped(cell: NewsTableViewCell) {
        self.tableView.reloadData()
    }
    
    func openBtnTapped(cell: NewsTableViewCell) {
        let detailVC = NewsDetailViewController(nibName: "NewsDetailViewController", bundle: nil)
        detailVC.newsData = (self.dataSource.data?[cell.indexPath.row])!
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
