//
//  VideoPlayerViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 07/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerViewController: BaseViewController {

    let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupPlayer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI()  {
        self.navigationItem.title = "PAK v IND"
        self.navigationController?.isNavigationBarHidden = false
        self.addSettingsButtonToNavigationBar()
    }
    
    func setupPlayer()  {
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player.play()
    }

}
