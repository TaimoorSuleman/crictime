//
//  LiveChannelsViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 20/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AVFoundation
import GoogleMobileAds

class LiveChannelsViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataSource = ChannelResultModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "LIVE CHANNELS"
        setupCollectionView()
//        getChannels()
        getLIVE()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Setup Methods
    func setupCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "LiveChannelsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LiveChannelsCollectionViewCell")
    }
    
    func playVideo(url:String, url_pol:String){
        let isEncrypted = url.components(separatedBy: ",").last
        let tokenUrl = url.components(separatedBy: ",").first
        let userName = url.components(separatedBy: ",")[1]
        let password = url.components(separatedBy: ",")[2]
        self.getToken(url: tokenUrl!, user: userName, pass: password, isEncrypt:isEncrypted!, url_Pol: url_pol)
    }
    
    
    func encrpytString(str:String)->String{
        var newString = str.replacingOccurrences(of: "eMeeea/1.0.0.", with: "")
        var count = 0
        
        for i in (0...newString.count-1).reversed() {
            if (count == 10 || count == 22 || count == 34 || count == 46 || count == 58) {
               
                newString =  (newString as NSString).replacingCharacters(in: NSRange(location: i, length: 1), with: "")
            }
            count = count + 1
        }
        
        return newString
    }
    
    
    func playVideo(str:String){
        

        let videoURL = URL(string: str)
        
        if (!AVAsset(url: videoURL!).isPlayable){
            print("-> Not a valid Url")
            return
        }
        
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        
        
        var adMobBannerView = GADBannerView()
        adMobBannerView = GADBannerView(adSize: kGADAdSizeLargeBanner)
        adMobBannerView.adUnitID = Constants.ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.load(GADRequest())
        
        adMobBannerView.translatesAutoresizingMaskIntoConstraints = false
        adMobBannerView.center = playerViewController.view.center
        playerViewController.view.addSubview(adMobBannerView)
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    //MARK: - API Methods
    func getChannels(){
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (response) in
            print(response)
            self.stopLoading()
            let decoder = JSONDecoder()
            do{
                self.dataSource = try decoder.decode(ChannelResultModel.self, from: response)
                self.collectionView.reloadData()
            }catch{
                print("Error")
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
        }
        self.startLoading()
        let params:Parameters = [:]
        APIManager.sharedInstance.getLiveChannels(parameters: params, success: successClosure, failure: failureClosure, errorPopup: false)
    }
    
    
    func getLIVE(){
        self.startLoading()
        let url = URL(string: Constants.liveChannelsURL)
        Alamofire.request(url!,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:nil)
            .validate()
            .responseString { response in
                self.stopLoading()
                if response.result.value != nil{
                    
                    let str =  response.result.value
                    let decryptedStr = Decrypt.convert(str)
                    let decoder = JSONDecoder()
                    do{
                        self.dataSource = try decoder.decode(ChannelResultModel.self, from: (decryptedStr?.data(using: .utf8))!)
                        self.collectionView.reloadData()
                    }catch{
                        print("Error")
                    }
                }else{
                 self.collectionView.reloadData()
                }
        }
        
        
    }
    
    func getToken(url:String,user:String,pass:String,isEncrypt:String,url_Pol:String){
        let user = user
        let password = pass
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        Alamofire.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:headers)
            .validate()
            .responseString { response in
                if response.result.value != nil{
                    let str =  response.result.value
                    var videoUrl = ""
                    if (isEncrypt == "true"){
                         videoUrl = url_Pol + self.encrpytString(str: str!)
                    }else{
                         videoUrl = url_Pol + str!
                    }
                    print("Video url =" + videoUrl + "2323")
                    self.playVideo(str: videoUrl)
                }else{
                    
                }
        }


    }

    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
  

}

//MARK: - UICollectionViewDataSource Methods
extension LiveChannelsViewController : UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.dataSource.status == 0 {
            Utility.emptyCollectionViewMessageWithImage(message: "Currently no match is playing", title: "Oooops", viewController: self, collectionView: self.collectionView)
            return 0
        }
        collectionView.backgroundView = UIView()
        return dataSource.data!.count
        
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "LiveChannelsCollectionViewCell", for: indexPath) as! LiveChannelsCollectionViewCell
        
        
            let model = dataSource.data![indexPath.row]
            
            cell.channelTitle.text = model.name
            cell.channelImage.sd_setImage(with: URL(string:(model.image )!), placeholderImage: #imageLiteral(resourceName: "Splash"))
        
        return cell
    }
    
}

//MARK: - UICollectionViewDelegate Methods
extension LiveChannelsViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        playVideo(url: dataSource.data![indexPath.row].live_id!,url_pol:dataSource.data![indexPath.row].url_pol!)
    }
}

//MARK: - UICollectionViewDelegateFlowLayout Methods
extension LiveChannelsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.size.width / 2
        let height = width + 20
        return CGSize(width: width, height: height)
    }
    
}
