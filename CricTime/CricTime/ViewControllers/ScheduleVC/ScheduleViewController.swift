//
//  ScheduleViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMobileAds

class ScheduleViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = FixtureListModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
//        getSchedule()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(dataSource.data?.count == 0){
            getSchedule()
        }
    }
    
    func setup(){
        self.navigationItem.title =  NSLocalizedString("Fixtures", comment: "")
        self.navigationController?.isNavigationBarHidden = false
//        self.addSettingsButtonToNavigationBar()
        tableView.register(UINib(nibName: "FixtureTableViewCell", bundle: nil), forCellReuseIdentifier: "FixtureTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getSchedule(){
        
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (response) in
            print(response)
            self.stopLoading()
            let decoder = JSONDecoder()
            do{
                self.dataSource = try decoder.decode(FixtureListModel.self, from: response)
                self.tableView.reloadData()
                print("abc")
            }catch{
                print("Error")
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
        }
        self.startLoading()
        let params:Parameters = ["":""]
        APIManager.sharedInstance.getSchedule(parameters: params, success: successClosure, failure: failureClosure, errorPopup: false)
        
    }

}

//MARK:- UITableView Datasource
extension ScheduleViewController:  UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataSource.status == 0 {
            Utility.emptyTableViewMessageWithImage(message: "Currently no schedule available", title: "Oooops", viewController: self, tableView: tableView)
            return 0
        }
        tableView.backgroundView = UIView()
        let adds = (dataSource.data?.count)! / 4
        return dataSource.data!.count / adds

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (dataSource.data?.count)! / 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: FixtureTableViewCell.self) as! FixtureTableViewCell
     
        let index = indexPath.section * 4 + indexPath.row % 5
        
        let team1Code = IsoCountryCodes.searchByName(name: (self.dataSource.data?[index].team1)!)
        
        var team1Name = team1Code.alpha3
        
        let team2Code = IsoCountryCodes.searchByName(name: (self.dataSource.data?[index].team2)!)
        var team2Name = team2Code.alpha3
        
        cell.matchDate.text = self.dataSource.data?[index].scheduled_on
        if(team1Name.isEmpty){
            team1Name = (self.dataSource.data?[index].team1)!
            cell.team1Name.text = team1Name
            cell.team1Flag.image = #imageLiteral(resourceName: "Splash")
            
        }else{
            cell.team1Name.text = team1Name
             cell.team1Flag.image = team1Code.flag.image()
        }
        
        if(team2Name.isEmpty){
            
            team2Name = (self.dataSource.data?[index].team2)!
            cell.team2Name.text = team2Name
            cell.team2Flag.image = #imageLiteral(resourceName: "Splash")
            
        }else{
            cell.team2Name.text = team2Name
             cell.team2Flag.image = team2Code.flag.image()
        }


        cell.team2Name.text = team2Name
        cell.venue.text = self.dataSource.data?[index].stadium
        
        if(self.dataSource.data?[indexPath.row].series?.contains("Cup"))!{
            cell.title.text = (self.dataSource.data?[index].match_type)! + " : " + (self.dataSource.data?[index].series)!
        }else{
            let vsString = team1Name + " VS " + team2Name
            cell.title.text = (self.dataSource.data?[index].match_type)! + " : " + vsString
        }
    
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var adMobBannerView = GADBannerView()
        adMobBannerView = GADBannerView(adSize: kGADAdSizeLargeBanner)
        adMobBannerView.adUnitID = Constants.ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.load(GADRequest())
        return adMobBannerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100//adMobBannerView.frame.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
}
