import UIKit
import Foundation
import AVKit
import AVFoundation
import Alamofire

class HomePageViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
   
    var dataSource : ChannelResultModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        getChannels()
    }
    
    func setup(){
        self.navigationItem.title =  NSLocalizedString("Home", comment: "")
        self.navigationController?.isNavigationBarHidden = false
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
       self.addSettingsButtonToNavigationBar()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getChannels(){
        
        
        let successClosure: DefaultArrayResultAPISuccessClosure = {
            (response) in
            print(response)
            self.stopLoading()
            let decoder = JSONDecoder()
            do{
                self.dataSource = try decoder.decode(ChannelResultModel.self, from: response)
                self.tableView.reloadData()
                print("abc")
            }catch{
                print("Error")
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            self.stopLoading()
        }
        self.startLoading()
        let params:Parameters = ["gotoNNPM": "',-1js'p;.,cyynnffs7712ruc'?#"]
        APIManager.sharedInstance.getLiveChannels(parameters: params, success: successClosure, failure: failureClosure, errorPopup: false)
        
    }
}

//MARK:- UITableView Datasource
extension HomePageViewController:  UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: HomeTableViewCell.self) as! HomeTableViewCell
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210.0
    }
    
}

extension HomePageViewController : HomeCellProtocol{
    func liveBtnTapped(cell: HomeTableViewCell) {
        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    
}
