//
//  VideoViewController.swift
//  CricTime
//
//  Created by Taimoor Suleman on 06/08/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VideoViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setup(){
        self.navigationItem.title =  NSLocalizedString("Recent Videos", comment: "")
        self.navigationController?.isNavigationBarHidden = false
        self.addSettingsButtonToNavigationBar()
        tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
    }

}

//MARK:- UITableView Datasource
extension VideoViewController:  UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: NewsTableViewCell.self) as! NewsTableViewCell
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
extension VideoViewController : NewsTableViewCellDelegate{
    func newsBtnTapped(cell: NewsTableViewCell) {
        self.tableView.reloadData()
    }
    
    func openBtnTapped(cell: NewsTableViewCell) {
        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}
