//
//  TestContraint.swift
//  CricTime
//
//  Created by Taimoor Suleman on 26/02/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class DynamicVerticalConstraint: NSLayoutConstraint {
    
    
    override init() {
        super.init()
        updateConstant()
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateConstant()
    }
    //It will send contraint constant according to devices. For this you have to set your contraint according to iPhone6
    func updateConstant(){
        
        self.constant = self.constant * (UIScreen.main.bounds.height / 667)
        
    }
}

class DynamicHorizontalConstraint: NSLayoutConstraint {
    
    
    override init() {
        super.init()
        updateConstant()
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateConstant()
    }
    //It will send contraint constant according to devices. For this you have to set your contraint according to iPhone6
    func updateConstant(){
        self.constant = self.constant * (UIScreen.main.bounds.height / 375)
    }
}
