

import UIKit
import Alamofire
import SwiftyJSON
class AuthenticationAPIManager: APIManagerBase {
    
    
    
    func getNews(parameters:Parameters,
        success:@escaping DefaultArrayResultAPISuccessClosure,
        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.NewsURL)){
            let route =  components.url! as URL
            self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
        }
    }
    
    func getSchedule(parameters:Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.SHCHEDULEURL)){
            let route =  components.url! as URL
            self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
        }
    }
    
    func getLiveChannelList(parameters:Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.liveChannelsURL)){
            let route =  components.url! as URL
    
            self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
        }
    }
    
}
