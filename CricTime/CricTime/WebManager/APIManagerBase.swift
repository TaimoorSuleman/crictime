import UIKit
import Alamofire
import SwiftyJSON


class APIManagerBase: NSObject
{
    var alamoFireManager : SessionManager!
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let defaultError = NSError(domain: "ACError", code: 0, userInfo: [NSLocalizedDescriptionKey: "Request Failed."])
    
    override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func getAuthorizationHeader () -> Dictionary<String,String>{
        return ["Content-Type":"application/json"]
    }
    
    func getVerificationHeader () -> Dictionary<String,String>{
        
        
        return ["Content-Type":"application/json"]
    }
    
    
    func getErrorFromResponseData(data: Data) -> NSError? {
        do{
            let result = try JSONSerialization.jsonObject(with: data,options: JSONSerialization.ReadingOptions.mutableContainers) as? Array<Dictionary<String,AnyObject>>
            if let message = result?[0]["message"] as? String{
                let error = NSError(domain: "GCError", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
                return error;
            }
        }catch{
            NSLog("Error: \(error)")
        }
        
        return nil
    }
    
    
    func postRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultArrayResultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure,
                         errorPopup: Bool){
        
        
        
        alamoFireManager.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON{
            response in
            
            
            
            guard response.result.error == nil else{
                
                print("error in calling post request")
                
                if errorPopup {self.showErrorMessage(error: response.result.error!)}

                failure(response.result.error as! NSError)
                return;
            }
            
            
            
            if let value = response.result.value {
                print (value)
                success(response.data!)
                
            }
        }
        
        
    }
    
    func postRequestWithBearer(route: URL,parameters: Parameters,
                               success:@escaping DefaultArrayResultAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure,
                               errorPopup: Bool){
        
        
        
        
        alamoFireManager.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getVerificationHeader()).responseJSON{
            response in
            
            
            
            guard response.result.error == nil else{
                
                print("error in calling post request")
                if errorPopup {self.showErrorMessage(error: response.result.error!)}

                failure(response.result.error as! NSError)
                return;
            }
            
            
            
            if let value = response.result.value {
                print (value)
                success(response.data!)
                
            }
            
            
            
        }
        
        
    }
    
    func postVerificationRequestWith(route: URL,parameters: Parameters,
                                     success:@escaping DefaultArrayResultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure,
                                     errorPopup: Bool){
        
        
        
        
        
        alamoFireManager.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getVerificationHeader()).responseJSON{
            response in
            
            
            
            guard response.result.error == nil else{
                
                print("\n- Error in calling post request")
                if errorPopup {self.showErrorMessage(error: response.result.error!)}

                failure(response.result.error as! NSError)
                return;
            }
            
            
            
            if let value = response.result.value {
                print (value)
                success(response.data!)
                
            }
            
            
            
        }
        
        
    }
    
    
    func showErrorMessage(error: Error)
    {
    }
    
    
    func getRequestWith(route: URL,parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        
        
        alamoFireManager.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
            response in
            
            
            guard response.result.error == nil else{
                
                print("error in calling post request")
                if errorPopup {self.showErrorMessage(error: response.result.error!)}
                failure(response.result.error! as NSError)
                
                return;
            }
            
            
            if let value = response.result.value {
                print (value)
                success(response.data!)
                
            }
          
            
            
            
        }
        
    }
    
    
    
    
    func putRequestWith(route: URL,parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        Alamofire.request(route, method: .put, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            
            
            guard response.result.error == nil else{
                
                print("error in calling post request")
                if errorPopup {self.showErrorMessage(error: response.result.error!)}

                failure(response.result.error! as NSError)
                return;
            }
            
            
            
            if let value = response.result.value {
                print (value)
                success(response.data!)
                
            }
            
            
            
        }
        
        
    }
    
    
    
    
    func deleteRequestWith(route: URL,parameters: Parameters,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure,
                           errorPopup: Bool){
        
        Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            
            
            guard response.result.error == nil else{
                
                print("error in calling post request")
                if errorPopup {self.showErrorMessage(error: response.result.error!)}

                failure(response.result.error! as NSError)
                return;
            }
            
            
            
            if let value = response.result.value {
                print (value)
                success(response.data!)
                
            }
            
            
            
        }
        
        
    }
    
    func requestWithMultipart(URLSTR: URLRequest, route: URL,parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure,
                              errorPopup: Bool){
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if parameters.keys.contains("photobase64") {
                let fileURL = URL(fileURLWithPath: parameters["photobase64"] as! String)
                multipartFormData.append(fileURL, withName: "profile_picture", fileName: "image.png", mimeType: "image/png")
            }
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                if ((keys[i] != "photobase64") && (keys[i] != "images")) {
                    subParameters[keys[i]] = values[i] as AnyObject
                }
            }
            
            if parameters.keys.contains("images") {
                let images = parameters["images"] as! Array<String>
                for i in 0  ..< images.count {
                    let fileURL = URL(fileURLWithPath: images[i])
                    multipartFormData.append(fileURL, withName: "image\(i+1)", fileName: "image\(i).png", mimeType: "image/png")
                }
            }
            
            for (key, value) in subParameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                //debug
                print(value)
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        
                        print("error in calling post request")
                        if errorPopup {self.showErrorMessage(error: response.result.error!)}

                        failure(response.result.error! as NSError)
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        success(response.data!)
                        
                    }
                    
                }
            case .failure(let encodingError):
                if errorPopup {self.showErrorMessage(error: encodingError)}

                failure(encodingError as NSError)
            }
        })
    }
    
    func putRequestWithMultipart_General(route: URL,parameters: Parameters,
                                         success:@escaping DefaultArrayResultAPISuccessClosure,
                                         failure:@escaping DefaultAPIFailureClosure,
                                         errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.put, headers: getAuthorizationHeader())
        
        requestWithMultipart(URLSTR: URLSTR, route: route, parameters: parameters, success: success, failure: failure , errorPopup: errorPopup)
    }
    
    func postRequestWithMultipart_General(route: URL,parameters: Parameters,
                                          success:@escaping DefaultArrayResultAPISuccessClosure,
                                          failure:@escaping DefaultAPIFailureClosure,
                                          errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getAuthorizationHeader())
        
        requestWithMultipart(URLSTR: URLSTR, route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultArrayResultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure,
                                  errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getAuthorizationHeader())
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                //                if ((keys[i] != "file") && (keys[i] != "images")) {
                subParameters[keys[i]] = values[i] as AnyObject
            }
            
            
            for (key, value) in subParameters {
                if let data:Data = value as? Data {
                    
                    multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/png")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        if errorPopup {self.showErrorMessage(error: response.result.error!)}

                        print("error in calling post request")
                        failure(response.result.error as! NSError)
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        success(response.data!)
                        
                    }
                    
                }
            case .failure(let encodingError):
                if errorPopup {self.showErrorMessage(error: encodingError)}

                failure(encodingError as NSError)
            }
        })
    }
    
    func postRequestWithMultipartWithBearer(route: URL,parameters: Parameters,
                                            success:@escaping DefaultArrayResultAPISuccessClosure,
                                            failure:@escaping DefaultAPIFailureClosure,
                                            errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getVerificationHeader())
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                //                if ((keys[i] != "file") && (keys[i] != "images")) {
                subParameters[keys[i]] = values[i] as AnyObject
            }
            
            
            for (key, value) in subParameters {
                if let data:Data = value as? Data {
                    
                    multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/png")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        
                        print("error in calling post request")
                        if errorPopup {self.showErrorMessage(error: response.result.error!)}

                        failure(response.result.error as! NSError)
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        success(response.data!)
                        
                    }
                    
                }
            case .failure(let encodingError):
                if errorPopup {self.showErrorMessage(error: encodingError)}
                failure(encodingError as NSError)
            }
        })
    }
    
    
    fileprivate func multipartFormData(parameters: Parameters) {
        let formData: MultipartFormData = MultipartFormData()
        if let params:[String:AnyObject] = parameters as [String : AnyObject]? {
            for (key , value) in params {
                
                if let data:Data = value as? Data {
                    
                    formData.append(data, withName: "profile_picture", fileName: "image.png", mimeType: "image/png")
                } else {
                    formData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
            print("\(formData)")
        }
    }
    
    
    //MARK:- ENCRYPTION
    
    //  Converted to Swift 4 by Swiftify v4.1.6792 - https://objectivec2swift.com/
//    class func convert(_ str: String) -> String {
//        var str = str
//        var key = "cricT!me9834olp"
//        var keyLength: Int
//        var keyArray = [Int](repeating: 0, count: 100)
//        if (key == "") {
//            return str
//        }
//        let asc: Int = 32
//        let c = "\(asc)"
//        key = key.replacingOccurrences(of: c, with: "")
//        // NSLog(@"Str: %@",str);
//        //NSLog(@"Key: %@",key);
//        if str.count < 8 {
//            exit(0)
//        }
//        if key.count < 32 {
//            keyLength = key.count
//        } else {
//            keyLength = 32
//        }
//        //NSLog(@"Keylength: %d",keyLength);
//        for i in 0..<keyLength {
//            keyArray[i] = Int(key[key.index(key.startIndex, offsetBy: i)] & 0x1f)
//        }
//        var j: Int = 0
//        //NSLog( @"%d",[str length]);
//        for i in 0..<str.count {
//            let e = Int(str[str.index(str.startIndex, offsetBy: i)])
//            // NSLog(@"index:%d ",e);
//            let ii = i
//            let a: Int = 1
//            if e & 0xe0 != 0 {
//                str = (str as NSString).replacingCharacters(in: NSRange(location: ii, length: a), with: "\(e ^ keyArray[j])")
//                // NSLog(@"%@",str);
//            } else {
//                str = (str as NSString).replacingCharacters(in: NSRange(location: ii, length: a), with: "\(e)")
//                // NSLog(@"%@",str);
//            }
//            j += 1
//            if j == keyLength {
//                j = 0
//            }
//        }
//        //NSLog(@" String: %@",str);
//        return str
//    }

    
    
}

public extension Data {
    public var mimeType:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "image/jpeg";
            case 0x89:
                return "image/png";
            case 0x47:
                return "image/gif";
            case 0x49, 0x4D:
                return "image/tiff";
            case 0x25:
                return "application/pdf";
            case 0xD0:
                return "application/vnd";
            case 0x46:
                return "text/plain";
            default:
                print("mimeType for \(c[0]) in available");
                return "application/octet-stream";
            }
        }
    }
}



