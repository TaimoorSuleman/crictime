
import UIKit
import Alamofire
import SwiftyJSON
extension APIManager{
    
    func getNews(parameters:Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure,
                 errorPopup: Bool){
        
        authenticationManagerAPI.getNews(parameters:parameters, success: success, failure: failure, errorPopup: false)
    }
    
    func getSchedule(parameters:Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure,
                 errorPopup: Bool){
        
        authenticationManagerAPI.getSchedule(parameters:parameters, success: success, failure: failure, errorPopup: false)
    }
    
    func getLiveChannels(parameters:Parameters,
                 success:@escaping DefaultArrayResultAPISuccessClosure,
                 failure:@escaping DefaultAPIFailureClosure,
                 errorPopup: Bool){
        
        authenticationManagerAPI.getLiveChannelList(parameters:parameters, success: success, failure: failure, errorPopup: false)
    }
    
    
    
}
