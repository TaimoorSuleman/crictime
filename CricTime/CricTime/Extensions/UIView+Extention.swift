
import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func shadow(color: UIColor) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3.0
        layer.masksToBounds = false
    }
    
    func  addRoundShadow(){
        let shadowSize :CGFloat = 10.0
        layer.cornerRadius = 10.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.clear.cgColor
        layer.masksToBounds = true;
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width:0,height: 1)
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false;
//        layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: -shadowSize / 2,
//                                                            y: -shadowSize / 2,
//                                                            width: self.frame.size.width,
//                                                            height: self.frame.size.height + shadowSize), cornerRadius:10.0).cgPath
    }
    
}
