//
//  TableView.swift
//  CricTime
//
//  Created by Taimoor on 05/01/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation



extension UITableView
{
    //Nib name and reusable identifier should be same
    func dequeueReusableCell(ofType: Any) -> UITableViewCell
    {
        self.register(UINib(nibName: String(describing: ofType), bundle: nil), forCellReuseIdentifier: String(describing: ofType))
        self.tableFooterView = UIView()

        return self.dequeueReusableCell(withIdentifier: String(describing: ofType))!
    }
    
    
    
}


