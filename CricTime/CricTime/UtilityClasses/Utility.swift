import Foundation
import UIKit
import MapKit
import AVFoundation
import Alamofire

class Utility {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }
    static func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    
    static func showAlert(title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { _ in })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    
    static func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    static func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    
    static func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
 
    static func getStarImage(_ starNumber: Double, _  rating: Double) -> UIImage {
        
        if round(rating) >= starNumber {
            return #imageLiteral(resourceName: "rate_yellowstar")
        } else {
            return #imageLiteral(resourceName: "rate_whitestar")
        }
    }
    
    static func timeParser(_ str: String) ->String
    {
        if(str != nil)
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dater = dateFormatter.date(from: "2017-08-11 "+str)
            
            dateFormatter.dateFormat = "h:mm a"
            let dateString = dateFormatter.string(from: dater!)
            return dateString.lowercased()
        }
        else
        {
            return ""
        }
        
    }



    

    
    static func getResponse(_ result: Dictionary<String,AnyObject>) -> String
    {
        return result["Message"] as! String
    }
    
    static func getErrorMessage(_ result: Dictionary<String,AnyObject>) -> String
    {
        return (result["Result"] as! Dictionary<String, Any>)["ErrorMessage"] as! String
    }
    
    
    static func applyBlurEffectToView(toView: UIView) -> UIView? {
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            toView.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = toView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.alpha = 0.85
            toView.addSubview(blurEffectView)
            
            return blurEffectView
        } else {
            toView.backgroundColor = UIColor.black
            return nil
        }
    }
    
    
    class func locale(for fullCountryName : String) -> String {
        var locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let identifier = NSLocale(localeIdentifier: localeCode)
            let countryName = identifier.displayName(forKey: NSLocale.Key.countryCode, value: localeCode)
            if fullCountryName.lowercased() == countryName?.lowercased() {
                return localeCode as! String
            }
        }
        return locales
    }
    
    class func emptyTableViewMessageWithImage(message:String,title: String, viewController: UIViewController, tableView: UITableView) {
        
        let noResultsView = EmptyTableViewBackgroundView.instanceFromNib()
        
        noResultsView.messageLabel.text = message
        noResultsView.titleLabel.text = title
        tableView.backgroundView = noResultsView
        tableView.separatorStyle = .none
    }
    
    class func emptyCollectionViewMessageWithImage(message:String,title: String, viewController: UIViewController, collectionView: UICollectionView) {
        
        let noResultsView = EmptyTableViewBackgroundView.instanceFromNib()
        
        noResultsView.messageLabel.text = message
        noResultsView.titleLabel.text = title
        collectionView.backgroundView = noResultsView
        
    }
    
    class func setupTabBarController(tabbarController: UITabBarController) {
        
        
        
        let scheduleViewController = ScheduleViewController()
        let videosViewController = VideoViewController()
        let homeViewController = LiveChannelsViewController()
        let newsViewController = NewsViewController()
        let favViewController = FavouriteViewController()
        
        
        
        // Creating navigation Controller and putting them in tabBarController because without it we will not be able to push viewController
        
        let scheduleNavigationController = UINavigationController()
        let videosNavigationController = UINavigationController()
        let homeNavigationController = UINavigationController()
        let newsNavigationController = UINavigationController()
        let favNavigationController = UINavigationController()
        
        scheduleNavigationController.viewControllers = [scheduleViewController]
        videosNavigationController.viewControllers = [videosViewController]
        homeNavigationController.viewControllers = [homeViewController]
        newsNavigationController.viewControllers = [newsViewController]
        favNavigationController.viewControllers = [favViewController]
        
        
        
        scheduleNavigationController.navigationBar.isTranslucent = false
        videosNavigationController.navigationBar.isTranslucent = false
        homeNavigationController.navigationBar.isTranslucent = false
        newsNavigationController.navigationBar.isTranslucent = false
        favNavigationController.navigationBar.isTranslucent = false
        
        
        
        tabbarController.viewControllers = []
        
        tabbarController.tabBar.isTranslucent = false
        
//        tabbarController.viewControllers = [scheduleNavigationController, videosNavigationController, homeNavigationController, newsNavigationController,favNavigationController]

                tabbarController.viewControllers = [scheduleNavigationController, homeNavigationController, newsNavigationController]
        tabbarController.selectedIndex = 1
        
//        tabbarController.tabBar.items![0].image = #imageLiteral(resourceName: "tab-schedule-unselect").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![1].image = #imageLiteral(resourceName: "tab-video-unselect").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![2].image = #imageLiteral(resourceName: "tab-home-unselect").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![3].image = #imageLiteral(resourceName: "tab-news-unselect").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![4].image = #imageLiteral(resourceName: "tab-favourite-unselect").withRenderingMode(.alwaysOriginal)
//
//        tabbarController.tabBar.items![0].selectedImage = #imageLiteral(resourceName: "tab-schedule-selected").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![1].selectedImage = #imageLiteral(resourceName: "tab-video-selected").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![2].selectedImage = #imageLiteral(resourceName: "tab-home-selected").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![3].selectedImage = #imageLiteral(resourceName: "tab-news-selected").withRenderingMode(.alwaysOriginal)
//        tabbarController.tabBar.items![4].selectedImage = #imageLiteral(resourceName: "tab-favourite-selected").withRenderingMode(.alwaysOriginal)
        
        tabbarController.tabBar.items![0].image = #imageLiteral(resourceName: "tab-schedule-unselect").withRenderingMode(.alwaysOriginal)
        tabbarController.tabBar.items![1].image = #imageLiteral(resourceName: "tab-home-unselect").withRenderingMode(.alwaysOriginal)
        tabbarController.tabBar.items![2].image = #imageLiteral(resourceName: "tab-news-unselect").withRenderingMode(.alwaysOriginal)
        
        tabbarController.tabBar.items![0].selectedImage = #imageLiteral(resourceName: "tab-schedule-selected").withRenderingMode(.alwaysOriginal)
        tabbarController.tabBar.items![1].selectedImage = #imageLiteral(resourceName: "tab-home-selected").withRenderingMode(.alwaysOriginal)
        tabbarController.tabBar.items![2].selectedImage = #imageLiteral(resourceName: "tab-news-selected").withRenderingMode(.alwaysOriginal)
        
        
        tabbarController.tabBar.barTintColor = UIColor.white
        tabbarController.tabBar.tintColor = UIColor.white
        tabbarController.tabBar.itemPositioning = .centered
        
        for (index,tabBarItem) in tabbarController.tabBar.items!.enumerated(){
            tabBarItem.title = ""
            if(index==1){
              tabBarItem.imageInsets = UIEdgeInsetsMake(-4, 0, 4, 0)
            }else{
                tabBarItem.imageInsets = UIEdgeInsetsMake(9, 0, -9, 0)
            }
        }
    }
    
}

