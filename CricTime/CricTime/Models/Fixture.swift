//
//  Fixture.swift
//  CricTime
//
//  Created by Taimoor Suleman on 08/09/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

class FixtureListModel : Codable {
   
    var status : Int? = 0
    var data : [FixtureModel]? = [FixtureModel]()
}


class FixtureModel : Codable {
    var series : String? = ""
    var scheduled_on : String? = ""
    var match_type : String? = ""
    var stadium : String? = ""
    var team1 : String? = ""
    var team2 : String? = ""
    var begin_at : String? = ""
}
