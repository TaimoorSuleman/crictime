//
//  NewsModel
//  Template
//
//  Created by Taimoor on 9/22/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit


struct ResultModel : Codable{
    var status : Int? = 0
    var data : [NewsModel]? = [NewsModel]()
}

struct NewsModel : Codable {
    var id : String? = ""
    var cid : String? = ""
    var image : String? = ""
    var title : String? = ""
    var description : String? = ""
}


struct ChannelResultModel : Codable {
    var status : Int? = 0
    var data : [ChannelsModel]? = [ChannelsModel]()
}

class ChannelsModel : Codable {
    var name : String? = ""
    var image : String? = ""
    var live_id : String? = ""
    var url_pol : String? = ""
}

