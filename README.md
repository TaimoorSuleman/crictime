TEMPLATE

DESCRIPTION
------------------------------
App consists of following major modules:

1. Home
2. Notifications
3. My Profile
4. Settings
5. Contact Us
6. About Us
7. Logout



RESOURCES
------------------------------
- ABC
- XYZ

Each resource is responsible and capable to do changes for his module.

ACCOUNTS
------------------------------
App Test Login Accounts:

 User Name:  JamesJHudson@yopmail.com
 Password: Gmail123


VERSION REQUIREMENTS
------------------------------
iOS 10+ and iPhone 6 or above

THIRD PARTY LIBRARIES/ FRAMEWORKS
------------------------------
[PODS]
- Alamofire
- SwiftyJSON
- IQKeyboardManagerSwift
- EZAlertController
- TPKeyboardAvoiding'
- RealmSwift
- Device
- SwiftValidator
- NVActivityIndicatorView
- SwiftMessages
- Kingfisher
- DZNEmptyDataSet
- Reachability
- EAIntroView
- UIAlertController+Blocks
- RESideMenu
- CountryPickerSwift
- KMPlaceholderTextView
- SkeletonView

[THIRD PARTY]
- Reachability


DEVELOPER NOTES
------------------------------
None

KNOWN ISSUES
------------------------------
None

PENDING CHANGE REQUESTS
------------------------------
None


CHANGE HISTORY
------------------------------

2017.12.11 - By James J. Hudson
+ Get request method added to pass same parameter as post request.
+ Webmanager class change to take time out. (You can set timeout under APIManagerBase init method).
+ IGStepper added for more customization
+ IGSegment Control added for customization
+ PagingHelper class is added to control paging in UITableViews and UICollectionViews (Example also added.)
+ IGUITextfield added for PickerView's problem.
+ Skeleton Added (This will remove self.startLoading())
+ Updated to Swift 4.0
+ Stable for iPhone X and XCode 9.1
+ Routes for WebManager is separated.
- Tutorial Screens removed.
- Extra pods removed



Copyright (C) 2017. All rights reserved.
